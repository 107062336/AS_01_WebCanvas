var canvas, ctx, flag = false,
    prevX = 0,
    currX = 0,
    prevY = 0,
    currY = 0,
    dot_flag = false,
    typing_flag = false,
    createtextbox,
    cur_color = "black",
    cur_typeface = "Calibri",
    cur_textsize = 20,
    y = 12,
    mode = "paint",
    index = 0,
    slide = [];


function ModeSel(cur_mode) {
    mode = cur_mode;
    if (mode == "paint") {
        canvas.style.cursor = 'url(images/paintBrush.png),auto';
    } else if (mode == "erase") {
        canvas.style.cursor = 'url(images/eraser.png),auto';
    } else if (mode == "text") {
        canvas.style.cursor = 'text';
    } else if (mode == "rect") {
        canvas.style.cursor = 'url(images/square.png),auto';
    } else if (mode == "tri") {
        canvas.style.cursor = 'url(images/triangle.png),auto';
    } else if (mode == "cir") {
        canvas.style.cursor = 'url(images/circle.png),auto';
    } else {
        canvas.style.cursor = "initial";
    }
}

function init() {
    canvas = document.getElementById("myCanvas");
    ctx = canvas.getContext("2d");
    w = canvas.width;
    h = canvas.height;
    slide[0] = ctx.getImageData(0, 0, canvas.width, canvas.height);
    canvas.addEventListener("mousemove", function(e) {
        findxy('move', e)
    }, false);
    canvas.addEventListener("mousedown", function(e) {
        findxy('down', e)
    }, false);
    canvas.addEventListener("mouseup", function(e) {
        findxy('up', e)
    }, false);
    canvas.addEventListener("mouseout", function(e) {
        findxy('out', e)
    }, false);
    if (mode == "paint") {
        canvas.style.cursor = 'url(images/paintBrush.png),auto';
    } else {
        canvas.style.cursor = "initial";
    }
}

function draw() {
    ctx.beginPath();
    ctx.moveTo(prevX, prevY);
    ctx.lineTo(currX, currY);
    ctx.strokeStyle = cur_color;
    ctx.lineWidth = (mode == "erase") ? parseInt(document.getElementById("myRange").value) + 15 : document.getElementById("myRange").value;
    ctx.lineCap = "round";
    ctx.stroke();
    ctx.closePath();
}

var o_x, o_y;

function findxy(res, e) {
    if (res == 'down') {
        ctx.lineCap = "round";
        ctx.lineJoin = "round";
        prevX = currX;
        prevY = currY;
        currX = (mode == "paint") ? e.clientX - canvas.offsetLeft - 5 : e.clientX - canvas.offsetLeft;
        currY = (mode == "paint") ? e.clientY - canvas.offsetTop + 20 : e.clientY - canvas.offsetTop;
        if (mode == "paint") {
            ctx.globalCompositeOperation = "source-over";
        } else if (mode == "erase") {
            ctx.globalCompositeOperation = "destination-out";
        } else if (mode == "text") {
            if (typing_flag == false) {
                myText();
                typing_flag = true;
            } else {
                stopTyping();
                typing_flag = false;
            }
        }
        flag = true;
        dot_flag = true;
        if (dot_flag && mode != "text") {
            ctx.beginPath();
            ctx.fillStyle = cur_color;
            ctx.fillRect(currX, currY, 1, 1);
            ctx.closePath();
            dot_flag = false;
        }
    } else if (res == 'up') {
        flag = false;
        var data = ctx.getImageData(0, 0, canvas.width, canvas.height);
        saveData(data);
    } else if (res == "out") {
        flag = false;
    } else if (res == 'move') {

        if (flag) {
            if (mode == "paint" || mode == "erase") {
                prevX = currX;
                prevY = currY;
                currX = (mode == "paint") ? e.clientX - canvas.offsetLeft - 5 : e.clientX - canvas.offsetLeft;
                currY = (mode == "paint") ? e.clientY - canvas.offsetTop + 20 : e.clientY - canvas.offsetTop;
                if (mode == "paint") {
                    draw();
                } else if (mode == "erase") {
                    draw();
                };
            } else if (mode == "rect") {
                ctx.putImageData(slide[index], 0, 0);
                prevX = e.offsetX;
                prevY = e.offsetY;
                ctx.beginPath();
                ctx.globalCompositeOperation = "source-over";
                ctx.lineWidth = 8;
                ctx.strokeStyle = cur_color;
                ctx.rect(currX, currY, prevX - currX, prevY - currY);
                ctx.stroke();
            } else if (mode == "tri") {

                ctx.putImageData(slide[index], 0, 0);
                prevX = e.offsetX;
                prevY = e.offsetY;
                len_x = prevX - currX;
                len_y = prevY - currY;
                ctx.beginPath();
                ctx.globalCompositeOperation = "source-over";
                ctx.lineWidth = 8;
                ctx.strokeStyle = cur_color;
                ctx.moveTo(prevX, prevY);
                ctx.lineTo(currX, currY);
                ctx.lineTo(currX + 2 * len_x, currY);
                ctx.closePath();
                ctx.stroke();
            } else if (mode == "cir") {
                ctx.putImageData(slide[index], 0, 0);
                prevX = e.offsetX;
                prevY = e.offsetY;
                ctx.beginPath();
                ctx.globalCompositeOperation = "source-over";
                ctx.lineWidth = 8;
                ctx.strokeStyle = cur_color;
                let radius = Math.sqrt((prevX - currX) ** 2 + (prevY - currY) ** 2) / 2;
                ctx.beginPath();
                ctx.arc((currX + prevX) / 2, (currY + prevY) / 2, radius, 0, Math.PI * 2);
                ctx.stroke();
            }
        }
    }
}

function clearall() {
    ctx.clearRect(0, 0, w, h);
    var data = ctx.getImageData(0, 0, canvas.width, canvas.height);
    saveData(data);
}

function myText() {
    createtextbox = document.createElement("INPUT");
    createtextbox.setAttribute("type", "text");
    createtextbox.style.position = 'absolute';
    createtextbox.style.left = currX + canvas.offsetLeft + "px";
    createtextbox.style.top = currY + canvas.offsetTop + "px";
    document.body.appendChild(createtextbox);
}

function stopTyping() {
    var myWord = createtextbox.value;
    createtextbox.style.visibility = "hidden";
    ctx.font = cur_textsize + "px " + cur_typeface;
    ctx.globalCompositeOperation = "source-over";
    ctx.strokeStyle = cur_color;
    ctx.fillText(myWord, prevX, prevY + 10);
}

function getTypeFace(sel) {
    cur_typeface = sel.value;
}

function getTextSize(sel) {
    cur_textsize = sel.value;
}

function pickColor() {
    document.getElementById('myColor').click();
    document.getElementById("myColor").onchange = function() {
        cur_color = document.getElementById("myColor").value;
    };
}

function download() {
    var download = document.getElementById("download");
    var image = document.getElementById("myCanvas").toDataURL("image/png").replace("image/png", "image/octet-stream");
    download.setAttribute("href", image);
}

function upload(event) {
    var img = new Image();
    img.src = URL.createObjectURL(event.target.files[0]);
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    img.onload = function() {
        ctx.globalCompositeOperation = "source-over";
        ctx.drawImage(img, 10, 10);
    }
}

function saveData(data) {
    (slide.length === 50) && (slide.shift());
    index = index + 1;
    slide[index] = data;
    max_index = index;
}

function Undo() {
    if (index <= 1) {
        ctx.putImageData(slide[0], 0, 0);
        if (index == 1) index--;
    } else {
        ctx.putImageData(slide[index - 1], 0, 0);
        index--;
    }
}

function Redo() {
    if (index < max_index) {
        ctx.putImageData(slide[index + 1], 0, 0);
        index = index + 1;
    }
}