# Software Studio 2020 Spring
## Assignment 01 Web Canvas
## 107062336 許程硯


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Others**                                       | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Appearance                                       | 5%        | Nice      |


---

### How to use 

1.      Paint brush to draw
2.      Palette to select color
3.      Eraser to erase
4.      Big "T" to type
5.      Counterclockwise arrow to undo
6.      Clockwise arrow to redo
7.      Square to draw rectangle
8.      Triangle to draw triangle
9.      Circle to draw circle
10.     Upward arrow to upload
11.     Downward arrow to download
12.     Double arrows to clear canvas


### Appearance

1.     Wood grain background
2.     Outset borders
3.     Marble toolbar
4.     Transparent icons and slider
5.     Icons light up when cursor is above

### Gitlab page link

    https://107062336.gitlab.io/AS_01_WebCanvas


<style>
table th{
    width: 100%;
}
</style>